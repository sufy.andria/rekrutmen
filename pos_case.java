package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class PosCase {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://dev-platform-recruitment.herokuapp.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testPosCase() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.name("fullname")).clear();
    driver.findElement(By.name("fullname")).sendKeys("lastri");
    driver.findElement(By.name("username")).clear();
    driver.findElement(By.name("username")).sendKeys("lastri123");
    driver.findElement(By.name("password")).clear();
    driver.findElement(By.name("password")).sendKeys("12345");
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys("lastri@gmail.com");
    driver.findElement(By.cssSelector("input.btn.btn-primary")).click();
    assertEquals(closeAlertAndGetItsText(), "Record has been Inserted..!!");
    driver.findElement(By.cssSelector("button.btn.btn-info")).click();
    driver.findElement(By.name("username")).clear();
    driver.findElement(By.name("username")).sendKeys("lastri123");
    driver.findElement(By.name("password")).clear();
    driver.findElement(By.name("password")).sendKeys("12345");
    driver.findElement(By.cssSelector("input.btn.btn-primary")).click();
    assertEquals(closeAlertAndGetItsText(), "Sukses Login");
    driver.findElement(By.linkText("Account")).click();
    driver.findElement(By.linkText("Job List")).click();
    driver.findElement(By.linkText("Home")).click();
    driver.findElement(By.linkText("Logout")).click();
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
